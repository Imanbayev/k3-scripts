var moment = require('moment');

const { Client, logger } = require('camunda-external-task-client-js');
const BearerTokenInterceptor = require('camunda-external-task-bearer-token-interceptor');
const { Variables } = require("camunda-external-task-client-js");

// ################ Keycloak ##########################
let bearerTokenInterceptor = null;
if (process.env.KEYCLOAK_CLIENT_ID && process.env.KEYCLOAK_CLIENT_SECRET && process.env.KEYCLOAK_BASE_URL && process.env.KEYCLOAK_REALM_ID) {
  bearerTokenInterceptor = new BearerTokenInterceptor({
    clientId: process.env.KEYCLOAK_CLIENT_ID,
    clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
    baseUrl: process.env.KEYCLOAK_BASE_URL,
    readlmId: process.env.KEYCLOAK_REALM_ID,
  });
}  

// ################ Camunda ##########################
const config = { baseUrl: process.env.CAMUNDA_URL, use: logger, interceptors: bearerTokenInterceptor, workerId: 'workflow-messages', interval: 1000 };
//const config = { baseUrl: 'http://localhost:8080/camunda/rest', use: logger };
const client = new Client(config);

var GoogleSpreadsheet = require('google-spreadsheet');
var doc = new GoogleSpreadsheet('1Nw2m2UYuHiHKnBWgIH7Z5HPfw2WQVnb2EDqD-6xmRCg');
var creds = require('./client_secret.json');
var async = require('async');
var sheet;

const axios = require('axios')
const smsUrl = 'http://212.124.121.186:9501/api?'
const smsUser = 'beintech'
const smsPass = 'RmB2l49F'
//const daysBefore = 7

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Заявка на организацию поездки. Забор данных с excel
client.subscribe('app-get-data', async function({ task, taskService }) {

  const emp = task.variables.getAllTyped().starter.value

  async.series([
    function setAuth(step) {
      doc.useServiceAccountAuth(creds, step);
    },
    function getInfoAndWorksheets(step) {
      doc.getInfo(function(err, info) {
        console.log('Loaded doc: '+info.title+' by '+info.author.email);
        sheet = info.worksheets[0];
        console.log('sheet 1: '+sheet.title+' '+sheet.rowCount+'x'+sheet.colCount);
        step();
      });
    },
    function workingWithRows(step) {
      sheet.getRows({
        offset: 1,
        query: `userid == ${emp}`
      }, function( err, rows ){

        if (rows != null){
          const variables = new Variables().setAllTyped({
            fio: {
              value: rows[0].fio,
              type: 'String',
            },
            phone: {
              value: rows[0].phone,
              type: 'String',
            },
            position: {
              value: rows[0].position,
              type: 'String',
            },
            backToBack: {
              value: rows[0].backtoback,
              type: 'String',
            },
            originPlace: {
              value: rows[0].originplace,
              type: 'String',
            },
            hotel1: {
              value: rows[0].hotel1,
              type: 'String',
            },
            hotel2: {
              value: rows[0].hotel2,
              type: 'String',
            },
          })
          taskService.complete(task, variables).then((result) => {
            console.log(`Result: ${result}`)
          })
        }
      });
    }
  ], function(err){
      if( err ) {
        console.log('Error: '+err);
      }
  });

});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Заявка на организацию поездки. Сохранение данных в excel
client.subscribe('app-save-data', async function({ task, taskService }) {

  const typedValues = task.variables.getAllTyped();
  var emp = typedValues.starter.value

  async.series([
    function setAuth(step) {
      doc.useServiceAccountAuth(creds, step);
    },
    function getInfoAndWorksheets(step) {
      doc.getInfo(function(err, info) {
        console.log('Loaded doc: '+info.title+' by '+info.author.email);
        sheet = info.worksheets[0];
        console.log('sheet 1: '+sheet.title+' '+sheet.rowCount+'x'+sheet.colCount);
        step();
      });
    },
    function workingWithRows(step) {
      sheet.getRows({
        offset: 1,
        query: `userid == ${emp}`
      }, function( err, rows ){

        try {
          rows[0].mobilizationdate = moment(typedValues.mobilizationDate.value).format('DD.MM.YYYY');
          rows[0].demobilizationdate = moment(typedValues.demobilizationDate.value).format('DD.MM.YYYY');
          rows[0].departurepoint1 = typedValues.departurePoint1.value;
          rows[0].camp1 = typedValues.camp1.value;
          rows[0].transport1 = typedValues.transport1.value;
          rows[0].from1 = typedValues.from1.value;
          if (moment(typedValues.departureDate1.value).format('DD.MM.YYYY') != moment(new Date("1970-01-01")).format('DD.MM.YYYY')) {
            rows[0].departuredate1 = moment(typedValues.departureDate1.value).format('DD.MM.YYYY');
          }
          rows[0].hotel1 = typedValues.hotel1.value;
          if (moment(typedValues.checkInDate1.value).format('DD.MM.YYYY') != moment(new Date("1970-01-01")).format('DD.MM.YYYY')) {
            rows[0].checkin1 = moment(typedValues.checkInDate1.value).format('DD.MM.YYYY');
          }
          if (moment(typedValues.checkOutDate1.value).format('DD.MM.YYYY') != moment(new Date("1970-01-01")).format('DD.MM.YYYY')) {
            rows[0].checkout1 = moment(typedValues.checkOutDate1.value).format('DD.MM.YYYY');
          }
          rows[0].departurepoint2 = typedValues.departurePoint2.value;
          rows[0].camp2 = typedValues.camp2.value;
          rows[0].transport2 = typedValues.transport2.value;
          rows[0].from2 = typedValues.from2.value;
          if (moment(typedValues.departureDate2.value).format('DD.MM.YYYY') != moment(new Date("1970-01-01")).format('DD.MM.YYYY')) {
            rows[0].departuredate2 = moment(typedValues.departureDate2.value).format('DD.MM.YYYY');
          }
          rows[0].hotel2 = typedValues.hotel2.value;
          if (moment(typedValues.checkInDate2.value).format('DD.MM.YYYY') != moment(new Date("1970-01-01")).format('DD.MM.YYYY')) {
            rows[0].checkin2 = moment(typedValues.checkInDate2.value).format('DD.MM.YYYY');
          }
          if (moment(typedValues.checkOutDate2.value).format('DD.MM.YYYY') != moment(new Date("1970-01-01")).format('DD.MM.YYYY')) {
            rows[0].checkout2 = moment(typedValues.checkOutDate2.value).format('DD.MM.YYYY');
          }
          rows[0].save();

          step();
        }
        catch (err) {
          console.log('Did not found row in excel');
        }

      });
    }
  ], function(err){
      if( err ) {
        console.log('Error: '+err);
      }
  });

  await taskService.complete(task);
});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Забор данных с excel при мобилизации
client.subscribe('mass-mobile-get-data', function({ task, taskService }) {
  const typedValues = task.variables.getAllTyped();
  var daysBefore = parseInt(typedValues.daysBefore.value)
  
  try {
    doc.useServiceAccountAuth(creds, (err) => {
      if (err) {
        taskService.handleFailure(task, "Authentication Error").then(result => {
          console.log(`Result: ${result}`);
        });
      } else {
        doc.getRows(1, (err, rows) => {
          var list = [];
          var isRows = 'NO'
          rows.forEach(element => {
            var shiftDate = moment(new Date(element.mobilizationdate.substring(3,5) + '.' + element.mobilizationdate.substring(0,2) + '.' + element.mobilizationdate.substring(6,10))).subtract(daysBefore, 'days').format('DD/MM/YYYY');
            if (shiftDate == moment(new Date()).format('DD/MM/YYYY')){
              list.push({
                startDate: element.mobilizationdate,
                endDate: element.demobilizationdate,
                fio: element.fio,
                userId: element.userid,
                phone: element.phone,
                email: element.email,
                position: element.position,
                backToBack: element.backtoback,
                originPlace: element.originplace,
                coordinatorPhone: element.coordinatorphone,
              });     

              isRows = 'YES'
            }
          });
        
          const variables = new Variables().setAllTyped({
            employees: {
              value: list,
              type: 'Json'
            },
            shiftDate: {
              value: moment(new Date()).add(daysBefore, 'days').format('DD/MM/YYYY'),
              type: 'String'
            },   
            isRows: {
              value: isRows,
              type: 'String'
            }   
          });

          taskService.complete(task, variables).then(result => {
            console.log(`Result: ${result}`);
          });

        });
      }
    });
  }
  catch (err){
    console.log(`Problem with excel: ${err}`);
  }

});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Забор данных с excel при демобилизации
client.subscribe('mass-demobile-get-data', function({ task, taskService }) {
  const typedValues = task.variables.getAllTyped();
  var daysBefore = parseInt(typedValues.daysBefore.value)

  try {
    doc.useServiceAccountAuth(creds, (err) => {
      if (err) {
        taskService.handleFailure(task, "Authentication Error").then(result => {
          console.log(`Result: ${result}`);
        });
      } else {
        doc.getRows(1, (err, rows) => {
          var list = [];
          var isRows = 'NO'
          rows.forEach(element => {
            var shiftDate = moment(new Date(element.demobilizationdate.substring(3,5) + '.' + element.demobilizationdate.substring(0,2) + '.' + element.demobilizationdate.substring(6,10))).subtract(daysBefore, 'days').format('DD/MM/YYYY');
            if (shiftDate == moment(new Date()).format('DD/MM/YYYY')){
              list.push({
                endDate: element.demobilizationdate,
                fio: element.fio,
                userId: element.userid,
                phone: element.phone,
                email: element.email,
                position: element.position,
                backToBack: element.backtoback,
                originPlace: element.originplace,
                coordinatorPhone: element.coordinatorphone,
              });     

              isRows = 'YES'
            }
          });
        
          const variables = new Variables().setAllTyped({
            employees: {
              value: list,
              type: 'Json'
            },
            shiftDate: {
              value: moment(new Date()).add(daysBefore, 'days').format('DD/MM/YYYY'),
              type: 'String'
            },
            isRows: {
              value: isRows,
              type: 'String'
            }   
          });

          taskService.complete(task, variables).then(result => {
            console.log(`Result: ${result}`);
          });

        });
      }
    });
  }
  catch (err){
    console.log(`Problem with excel: ${err}`);
  }

});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Сохранение данных в excel при мобилизации
client.subscribe('mass-mobile-save-data', async function({ task, taskService }) {

  const emp = task.variables.getAllTyped().employee.value;

  async.series([
    function setAuth(step) {
      doc.useServiceAccountAuth(creds, step);
    },
    function getInfoAndWorksheets(step) {
      doc.getInfo(function(err, info) {
        console.log('Loaded doc: '+info.title+' by '+info.author.email);
        sheet = info.worksheets[0];
        console.log('sheet 1: '+sheet.title+' '+sheet.rowCount+'x'+sheet.colCount);
        step();
      });
    },
    function workingWithRows(step) {
      sheet.getRows({
        offset: 1,
        query: `userid == ${emp.userId}`
      }, function( err, rows ){
        try {
          rows[0].mobilizationdate = emp.newStartDate;
          rows[0].demobilizationdate = emp.newEndDate;
          rows[0].save();
        }
        catch (err) {
          console.log('Did not found row in excel');
        }
        step();
      });
    }
  ], function(err){
      if( err ) {
        console.log('Error: '+err);
      }
  });

  await taskService.complete(task);
});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Сохранение данных в excel при демобилизации
client.subscribe('mass-demobile-save-data', async function({ task, taskService }) {

  const emp = task.variables.getAllTyped().employee.value;

  async.series([
    function setAuth(step) {
      doc.useServiceAccountAuth(creds, step);
    },
    function getInfoAndWorksheets(step) {
      doc.getInfo(function(err, info) {
        console.log('Loaded doc: '+info.title+' by '+info.author.email);
        sheet = info.worksheets[0];
        console.log('sheet 1: '+sheet.title+' '+sheet.rowCount+'x'+sheet.colCount);
        step();
      });
    },
    function workingWithRows(step) {
      sheet.getRows({
        offset: 1,
        query: `userid == ${emp.userId}`
      }, function( err, rows ){
        try {
          rows[0].demobilizationdate = emp.newEndDate;
          rows[0].save();
        }
        catch (err) {
          console.log('Did not found row in excel');
        }
        step();
      });
    }
  ], function(err){
      if( err ) {
        console.log('Error: '+err);
      }
  });

  await taskService.complete(task);
});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Проверка на наличие записей при мобилизации
client.subscribe('mass-mobile-check-data', function({ task, taskService }) {
  const typedValues = task.variables.getAllTyped();
  var transportDay = parseInt(typedValues.transportDay.value)

  try {
    doc.useServiceAccountAuth(creds, (err) => {
      if (err) {
        taskService.handleFailure(task, "Authentication Error").then(result => {
          console.log(`Result: ${result}`);
        });
      } else {
        doc.getRows(1, (err, rows) => {
          var isRows = 'NO'
          rows.forEach(element => {
            var shiftDate = moment(new Date(element.mobilizationdate.substring(3,5) + '.' + element.mobilizationdate.substring(0,2) + '.' + element.mobilizationdate.substring(6,10))).subtract(transportDay, 'days').format('DD/MM/YYYY');
            if (shiftDate == moment(new Date()).format('DD/MM/YYYY')){
              isRows = 'YES'
            }
          });
        
          const variables = new Variables().setAllTyped({
            isRows: {
              value: isRows,
              type: 'String'
            }   
          });

          taskService.complete(task, variables).then(result => {
            console.log(`Result: ${result}`);
          });

        });
      }
    });
  }
  catch (err){
    console.log(`Problem with excel: ${err}`);
  }

});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Проверка на наличие записей при демобилизации
client.subscribe('mass-demobile-check-data', function({ task, taskService }) {
  const typedValues = task.variables.getAllTyped();
  var transportDay = parseInt(typedValues.transportDay.value)

  try {
    doc.useServiceAccountAuth(creds, (err) => {
      if (err) {
        taskService.handleFailure(task, "Authentication Error").then(result => {
          console.log(`Result: ${result}`);
        });
      } else {
        doc.getRows(1, (err, rows) => {
          var isRows = 'NO'
          rows.forEach(element => {
            var shiftDate = moment(new Date(element.demobilizationdate.substring(3,5) + '.' + element.demobilizationdate.substring(0,2) + '.' + element.demobilizationdate.substring(6,10))).subtract(transportDay, 'days').format('DD/MM/YYYY');
            if (shiftDate == moment(new Date()).format('DD/MM/YYYY')){
              isRows = 'YES'
            }
          });
        
          const variables = new Variables().setAllTyped({
            isRows: {
              value: isRows,
              type: 'String'
            }   
          });

          taskService.complete(task, variables).then(result => {
            console.log(`Result: ${result}`);
          });

        });
      }
    });
  }
  catch (err){
    console.log(`Problem with excel: ${err}`);
  }

});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Отправка СМС сотрудникам при мобилизации
client.subscribe('mass-mobile-send-sms', function({ task, taskService }) {

  const typedValues = task.variables.getAllTyped();
  var transportDay = parseInt(typedValues.transportDay.value)
  try {
    doc.useServiceAccountAuth(creds, (err) => {
      if (err) {
        taskService.handleFailure(task, "Authentication Error").then(result => {
          console.log(`Result: ${result}`);
        });
      } else {
        doc.getRows(1, (err, rows) => {
          rows.forEach(element => {
            var shiftDate = moment(new Date(element.mobilizationdate.substring(3,5) + '.' + element.mobilizationdate.substring(0,2) + '.' + element.mobilizationdate.substring(6,10))).subtract(transportDay, 'days').format('DD/MM/YYYY');
            if (shiftDate == moment(new Date()).format('DD/MM/YYYY')){

              var sms_text = `${moment(typedValues.departureDate.value).format('DD/MM/YYYY')} otpravlenie avtobusa na Prorvu s aeroporta v 07:30; s ofisa Kazpaco na Satpaeva 23b v 08:00; s zh.d.vokzala v 08:45; s Kulsary v 12:55(kafe Ak Zhol). ${typedValues.transport.value}. Voditel' ${typedValues.driver.value}.`
              axios.get(smsUrl, {
                params: {
                  action: 'sendmessage',
                  username: smsUser,
                  password: smsPass,
                  recipient: element.phone,
                  messagetype: 'SMS:TEXT',
                  originator: 'BI',
                  messagedata: sms_text,
                }
              })
              .catch(function (error) {
                console.log(error);
              })
            }
          });

          taskService.complete(task).then(result => {
            console.log(`Result: ${result}`);
          });

        });
      }
    });
  }
  catch (err){
    console.log(`Problem with excel: ${err}`);
  }

});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Отправка СМС сотрудникам при демобилизации
client.subscribe('mass-demobile-send-sms', function({ task, taskService }) {

  const typedValues = task.variables.getAllTyped();
  var transportDay = parseInt(typedValues.transportDay.value)
  try {
    doc.useServiceAccountAuth(creds, (err) => {
      if (err) {
        taskService.handleFailure(task, "Authentication Error").then(result => {
          console.log(`Result: ${result}`);
        });
      } else {
        doc.getRows(1, (err, rows) => {
          rows.forEach(element => {
            var shiftDate = moment(new Date(element.demobilizationdate.substring(3,5) + '.' + element.demobilizationdate.substring(0,2) + '.' + element.demobilizationdate.substring(6,10))).subtract(transportDay, 'days').format('DD/MM/YYYY');
            if (shiftDate == moment(new Date()).format('DD/MM/YYYY')){

              var sms_text = `${moment(typedValues.departureDate.value).format('DD/MM/YYYY')} otpravlenie avtobusa v Atyrau iz kempa Unexstroi v 08:00; iz BI kempa 08:40. ${typedValues.transport.value}. Voditel' ${typedValues.driver.value}.`
              axios.get(smsUrl, {
                params: {
                  action: 'sendmessage',
                  username: smsUser,
                  password: smsPass,
                  recipient: element.phone,
                  messagetype: 'SMS:TEXT',
                  originator: 'BI',
                  messagedata: sms_text,
                }
              })
              .catch(function (error) {
                console.log(error);
              })

              element.mobilizationdate = moment(new Date()).add(29, 'days').format('DD.MM.YYYY');
              element.demobilizationdate = moment(new Date()).add(57, 'days').format('DD.MM.YYYY');
              element.save()
            }
          });

          taskService.complete(task).then(result => {
            console.log(`Result: ${result}`);
          });

        });
      }
    });
  }
  catch (err){
    console.log(`Problem with excel: ${err}`);
  }

});